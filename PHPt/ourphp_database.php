<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

namespace PHPt;

class ourphp_database
{
	public function __construct()
	{
		
	}
	
	public static function databaseinfo()
	{
		$database = [
						// 是否使用数据库  true 开启  false 关闭
						"openclose" => false, 
						//数据库链接类型   mysql 或 mysqli
						'mysqltype' => 'mysqli', 
						// 数据库链接地址
						"mysqlurl" => "127.0.0.1", 
						// 数据库登录账号
						"mysqlname" => "root", 
						// 数据库登录密码
						"mysqlpass" => "root", 
						// 数据库名
						"mysqldb" => "ourphp", 
		];
		return $database;
	}
}

?>