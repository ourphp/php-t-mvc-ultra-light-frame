<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

function ourphp_autoload($className)
{
	$file = WEB_ROOT.'/'.str_replace('\\', '/', $className).'.php';
	if(file_exists($file)){
		include $file;
	}
}

?>