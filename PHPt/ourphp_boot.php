<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/
header("Content-Type:text/html; charset=UTF-8"); 
header("Cache-Control:no-cache");

require_once(WEB_ROOT . 'PHPt' . OP . 'ourphp_config.php');
require_once(WEB_ROOT . 'PHPt' . OP . 'ourphp_autoload.php');

session_start();
date_default_timezone_set($config['timezone']);
spl_autoload_register("ourphp_autoload");


$app = new PHPt\ourphp_app;
$app -> run();

?>