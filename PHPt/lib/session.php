<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

namespace PHPt\lib;
use PHPt\ourphp_function;

class session
{
	
	public function __construct()
	{
		
	}
	
	public function SESSION($key = '', $info = '', $type = 1){
		if($key == '' || $type == '')
		{
			return false;
		}
		session_start();
		switch($type){
			case "1":
				$_SESSION[$key] = $info;
			break;
			case "2":
				if(empty($_SESSION[$key])){
					return false;
				}else{
					return $_SESSION[$key];
				}
			break;
			case "3":
				unset($_SESSION[$key]);
			break;
			case "4":
				if(isset($_SESSION[$key])){
					return true;
				}else{
					return false;
				}
			break;
		}
	}

	
}
?>