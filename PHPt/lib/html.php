<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

namespace PHPt\lib;
use PHPt\lib\style;

class html
{
	
	public function __construct()
	{
		
	}
	
	public static function E($content = '')
	{
		echo $content;
	}
	
	public static function I($file = '')
	{
		if($file == '')
		{
			echo style::E("加载模板地址不能为空");
			exit;
		}
		if(!file_exists($file))
		{
			echo style::E("模板 ".$file." 未找到");
			exit;
		}
		
		return $file;
	}
	
}
?>