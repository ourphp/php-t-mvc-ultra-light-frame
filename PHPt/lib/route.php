<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

namespace PHPt\lib;
use PHPt\ourphp_config;

class route
{
	
	public function __construct()
	{
		
	}
	
	public static function URL($url)
	{
		global $config;
		if($config['urlrewrite'])
		{
			$url = "/" . $url;
		}else{
			$url = "/index.php/" . $url;
		}
		return $url;
	}
	
}
?>