<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

namespace PHPt\lib;
use PHPt\ourphp_function;

class cookie
{
	
	public function __construct()
	{
		
	}
	
	public function COOKIE($key = '', $info = '', $type = 1){
		if($key == '' || $type == '')
		{
			return false;
		}
		switch($type){
			case "1":
				setcookie($key, $info, time() + 86400);
			break;
			case "2":
				if(empty($_COOKIE[$key])){
					return false;
				}else{
					return $_COOKIE[$key];
				}
			break;
			case "3":
				setcookie($key, "");
			break;
			case "4":
				if(isset($_COOKIE[$key])){
					return true;
				}else{
					return false;
				}
			break;
		}
	}

	
}
?>