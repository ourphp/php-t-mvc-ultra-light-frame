<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

namespace PHPt\lib;
use PHPt\ourphp_function;

class safe
{
	
	public function __construct()
	{
		
	}
	
	public function TOKEN($token = ''){
		global $config;
		if($token == ''){
			return false;

		}else{
			return MD5($token.$config['safecode']);
			
		}
	}
	
	public function HIDDENINPUT($str = '',$name = ''){
		global $config;
		if($str == '' || $name == ''){

			return false;

		}else{

		    $t = $this -> TOKEN($str);
			return '<input type="hidden" name="'.$name.'" id="'.$name.'" value="'.$str.'@'.$t.'">';

		}

	}
	
}
?>