<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

namespace PHPt\lib;

class style
{
	
	public function __construct()
	{
		
	}
	
	public static function E($font = '')
	{
		$html = '
				<style type="text/css">
					.style{
							width:86%;
							float:left;
							margin:20px 5%;
							padding:2%;
							background:#f4f4f4;
							border-radius:8px;
							color:#333;
					}
				</style>
				<div class="style">
				'.$font.'
				</div>
		';
		return $html;
	}
	
}
?>