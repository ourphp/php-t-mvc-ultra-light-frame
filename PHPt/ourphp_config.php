<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

$config = [

		'version' => '3.0.0',
		'update' => '20240312',
		'default_model' => 'home',
		'default_controller' => 'index',
		'default_function' => 'index',
		'timezone' => 'PRC',
		'urlrewrite' => false,
		'safecode' => 'e10adc3949ba59abbe56e057f20f883e',

];

?>