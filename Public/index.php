<?php
/**
 * PHP-T超轻量级PHP开发框架
 *
 * @author    哈尔滨伟成科技有限公司 QQ77701950
 * @copyright Copyright (c) 2023
 * @license   PHP-T 遵循Apache2开源协议发布，需保留开发者信息。
 * @link      http://www.ourphp.net
**/

//error_reporting(0);

define('PHPT', "PHP-T");
/*
*	系统目录分隔符
*/
define('OP', DIRECTORY_SEPARATOR);

/*
*	项目目录绝对路径（一）
*/
define('WEB_ROOT', realpath(__DIR__.OP.'..'.OP).OP);

/*
*	项目目录绝对路径（二）
	define('WEB_ROOT', dirname(__FILE__).OP);
*/


/*
*	应用命名空间
*/
define('APP_NAMESPACE', 'App');

/*
*	应用目录入口
*/
define('APP_ROOT', WEB_ROOT . 'App'.OP);

/*
*	启动php-t
*/
require(WEB_ROOT .'PHPt'.OP.'ourphp_boot.php');

?>